# Joystick controlled Pac-Man game on Nokia 5510 LCD

## Description
Programmed a joystick controlled multiplayer Pac-Man game for Nokia 5510 LCD using C language and an ARM®
Cortex™-M0+ processor based development board. FRDM-KL25Z development board was used as a platform. Nokia 5110 Screen - 84x48 Graphic LCD was used as the display. The Pac-Man game was implemented on Keil Uvision IDE using C language from stracth, no library was used during the design of game. SPI communucation protocole was used to communicate with the Nokia LCD display. 

## Visuals
The demo of the project is shown at the Youtube video:

https://www.youtube.com/watch?v=SL_nBfcHZbc


## Project status
Completed in 2021-2022 Spring Semester at Bilkent University as the term project of Microprocessors course.
